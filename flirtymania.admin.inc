<?php

/**
 * mrtigra config form.
 */
function flirtymania_config_form($form, &$form_state) {
  $form = array();
 
  $form['flirtymania_rid'] = array(
    '#type' => 'textfield',
    '#title' => t(flirtymania_lang("rid_header")),
    '#default_value' => variable_get('flirtymania_rid', ""),
    '#description' => t(flirtymania_lang("rid_descr")),
    '#required' => FALSE,
  );
  
  $form['flirtymania_description'] = array(
    '#type' => 'item',
    '#title' => flirtymania_lang("guide_header"),
    '#markup' => flirtymania_lang("guide"),
  );
  return system_settings_form($form);
}